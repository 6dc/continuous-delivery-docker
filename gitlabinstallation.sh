﻿echo "starting the gitlab-container"

echo "you can accsess gitlab throuh localhost:80 "

echo "The GitLab container uses host mounted volumes to store persistent data:
Local location 	Container location 	Usage
/srv/gitlab/data 	/var/opt/gitlab 	For storing application data
/srv/gitlab/logs 	/var/log/gitlab 	For storing logs
/srv/gitlab/config 	/etc/gitlab 	For storing the GitLab configuration files

You can fine tune these directories to meet your requirements."

 docker run --detach \
    --hostname gitlab.example.com \
    --publish 443:443 --publish 80:80 --publish 22:22 \
    --name gitlab \
    --restart always \
    --volume /srv/gitlab/config:/etc/gitlab \
    --volume /srv/gitlab/logs:/var/log/gitlab \
    --volume /srv/gitlab/data:/var/opt/gitlab \
    gitlab/gitlab-ce:latest

