docker run -d --name test1 -p 80:80 -p 8081:8081 -p 443:443 jfrog-docker-reg2.bintray.io/jfrog/artifactory-registry:latest

echo "When running Artifactory Pro with the port bindings described above, you can access Artifactory at the following URLs:
http://localhost/artifactory"